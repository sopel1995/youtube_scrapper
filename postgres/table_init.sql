CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE youtube_db (
    uuid UUID UNIQUE DEFAULT uuid_generate_v4(),
	link VARCHAR(1024) NULL,
	description VARCHAR(20480) NULL,
	name VARCHAR(1024) NULL,
	category VARCHAR(255) NULL,
	search_key VARCHAR(300) NULL,
	access_date VARCHAR(1024) NULL,
	add_date VARCHAR(1024) NULL
);

INSERT INTO youtube_db (uuid, link, description, name, category, search_key, access_date,
                      add_date)
VALUES ('452a5a3a-efda-4174-9b0f-cf12e584760f', 'link', 'description', 'name', 'category', 'search_key', 'access_date', 'add_date');

import logging
import os
from aiohttp import web
from app.data.wrapper import DbWrapper, DbWrapperParams
from app.web import WebApp

if __name__ == '__main__':
    host = os.getenv("HOST", "0.0.0.0")
    port = os.getenv("PORT", 6969)

    get_youtube_fields = os.environ.get("GET_YOUTUBE_FIELDS",
                                        'link,description,name,category,duration,access_date,add_date')
    get_youtube_fields = frozenset(get_youtube_fields.split(","))

    db_wrapper_params = DbWrapperParams(
        os.environ.get("DB_HOST", "127.0.0.1"),
        os.environ.get("DB_PORT", 5432),
        os.environ.get("DB_NAME", "youtube_db"),
        os.environ.get("DB_USER", ""),
        os.environ.get("DB_PASSWORD", ""),
        int(os.environ.get("DB_POLL_MIN_SIZE", 5)),
        int(os.environ.get("DB_POLL_MAX_SIZE", 10))
    )
    db_wrapper = DbWrapper(params=db_wrapper_params)
    logging.debug(db_wrapper is not None)
    db_engine_session, db_session = db_wrapper.init_db()
    logging.debug(db_session is not None)

    web_app = WebApp(db_session, get_youtube_fields)

    web.run_app(
        web_app.decorate_with_swagger(
            web_app.register_rest_endpoints()
        ),
        host=host,
        port=port
    )

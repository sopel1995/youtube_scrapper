FROM alpine:3.10

RUN mkdir app
COPY requirements.txt /app

RUN apk update \
    && apk add postgresql-dev gcc musl-dev git curl \
    && apk add --no-cache python3-dev \
    && pip3 install --upgrade pip \
    && python3 -m venv venv && . venv/bin/activate

RUN pip3 install -r app/requirements.txt
COPY ./app /app/app
COPY Makefile /app
COPY main.py /app
WORKDIR /app

ENTRYPOINT ["python3", "main.py"]

from aiohttp.web import Application
from aiohttp_swagger import setup_swagger

from app.rest.handlers.youtube import GeneralWebHandler


class WebApp:
    def __init__(self,
                 db_session: object,
                 get_attribute_fields: set,
                 logger=None):
        self.db_session = db_session
        self.get_attribute_fields = get_attribute_fields
        self.logger = logger

    def register_rest_endpoints(self):
        app = Application()
        general = GeneralWebHandler(self.logger, self.db_session)
        app.router.add_get('/', general.get_all_datebase, allow_head=False)
        app.router.add_post('/search_key/{search_key}', general.get_key)
        app.router.add_get('/get_saved_records_key/{search_key}', general.get_saved_records_key, allow_head=False)
        app.router.add_delete('/delete_key/{delete_key}', general.delete_database)

        return app

    @staticmethod
    def decorate_with_swagger(app: object):
        long_description = """
        Service responsible for youtube database.
        """
        setup_swagger(app,
                      description=long_description,
                      title="Youtube Scrapper",
                      api_version="0.1",
                      contact="Filip Kostecki & Mateusz Sobkowiak",
                      swagger_url="/api")
        return app

from sqlalchemy import Column, String, Integer
from sqlalchemy.dialects.postgresql import UUID

from app.model import BaseModel


class Youtube(BaseModel):
    __tablename__ = 'youtube_db'
    to_serialize = ['uuid', 'link', 'description', 'search_key', 'name', 'category', 'duration', 'access_date',
                    'add_date']
    uuid = Column(UUID(), primary_key=True, unique=True, nullable=False)
    search_key = Column(String(300), unique=False, nullable=False)
    link = Column(String(1024), unique=False, nullable=False)
    description = Column(String(20480), unique=False, nullable=False)
    name = Column(String(1024), unique=False, nullable=False)
    category = Column(String(255), unique=False, nullable=False)
    access_date = Column(String(1024), unique=False, nullable=False)
    add_date = Column(String(1024), unique=False, nullable=False)

    @classmethod
    def from_json(cls, data):
        return cls(**data)

    def __init__(self, link, description, name, search_key,
                 category, access_date, add_date):
        self.link = link
        self.description = description
        self.name = name
        self.search_key = search_key
        self.category = category
        self.access_date = access_date
        self.add_date = add_date

    def to_json(self):
        data = {}
        for attr_name in self.to_serialize:
            data[attr_name] = getattr(self, attr_name)
        return data

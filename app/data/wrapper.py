from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker


class DbWrapperParams:
    host = None
    port = None
    database = None
    user = None
    password = None
    min_size = None
    max_size = None

    def __init__(self, host, port, database, user, password, min_size, max_size):
        self.host = host
        self.port = port
        self.database = database
        self.user = user
        self.password = password
        self.min_size = min_size
        self.max_size = max_size

    @staticmethod
    def get_defaults():
        return DbWrapperParams(None, None, None, None, None, None, None)


class DbWrapper:
    def __init__(self, params=DbWrapperParams.get_defaults()):
        self.host = params.host
        self.port = params.port
        self.database = params.database
        self.user = params.user
        self.password = params.password
        self.min_size = params.min_size
        self.max_size = params.max_size

    def init_db(self):
        db_uri = f"postgresql+psycopg2://{self.user}:{self.password}@{self.host}:{self.port}/{self.database}"
        engine_session = sessionmaker(autocommit=False,
                                      autoflush=False,
                                      bind=create_engine(db_uri))
        session = scoped_session(engine_session)
        return engine_session, session

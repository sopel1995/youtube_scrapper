import inspect
from abc import ABC, abstractmethod

from sqlalchemy.exc import SQLAlchemyError

from aiohttp.http_exceptions import HttpBadRequest
from aiohttp.web_exceptions import HTTPMethodNotAllowed
from aiohttp.web_request import Request
from aiohttp.web_response import Response

from app.rest.json_response import JsonResponse

DEFAULT_METHODS = ('GET', 'POST', 'PUT', 'DELETE')


class RestEndpoint:
    def __init__(self):
        self.methods = {}

        for method_name in DEFAULT_METHODS:
            method = getattr(self, method_name.lower(), None)
            if method:
                self.register_method(method_name, method)

    def register_method(self, method_name, method):
        self.methods[method_name.upper()] = method

    async def dispatch(self, request: Request):
        method = self.methods.get(request.method.upper())
        if not method:
            raise HTTPMethodNotAllowed('', DEFAULT_METHODS)

        wanted_args = list(inspect.signature(method).parameters.keys())
        available_args = request.match_info.copy()
        available_args.update({'request': request})

        unsatisfied_args = set(wanted_args) - set(available_args.keys())
        if unsatisfied_args:
            # Expected match info that doesn't exist
            raise HttpBadRequest('')

        return await method(**{arg_name: available_args[arg_name] for arg_name in wanted_args})


class CollectionEndpointAbstract(RestEndpoint, ABC):

    def __init__(self, resource, session, model, uuid_generator):
        super().__init__()
        self.resource = resource
        self.session = session
        self.model = model
        self.uuid_generator = uuid_generator

    async def get(self, *args, **kwargs) -> Response:
        data = []

        try:
            results_data = self.session.query(self.model).all()

            for instance in self.resource.collection.values():
                data.append(self.resource.render(instance))

            self.resource.encode(data)

            return Response(status=200, body=self.resource.encode({
                'results': [result_data.to_json() for result_data in results_data]
            }), content_type='application/json')
        except SQLAlchemyError as err:
            self.session.rollback()
            return JsonResponse("{}", err, status=400)
        except Exception as err:
            self.session.rollback()
            return JsonResponse("Unexpected error: {}", err, status=500)

    @abstractmethod
    async def post(self, request, *args, **kwargs) -> Response:
        try:
            model = args[0]
        except KeyError as err:
            return JsonResponse("You need to add model parameter in endpoint implementation: {}", err, status=500)

        try:
            self.session.add(model)
            self.session.commit()
            return Response(status=201, body=self.resource.encode(model.to_json()),
                            content_type='application/json')
        except SQLAlchemyError as err:
            self.session.rollback()
            return JsonResponse("{}", err, status=400)
        except Exception as err:
            self.session.rollback()
            return JsonResponse("Unexpected error: {}", err, status=500)


class InstanceEndpointAbstract(RestEndpoint, ABC):
    MAX_ID_LENGTH = 36

    def __init__(self, resource, session, model):
        super().__init__()
        self.resource = resource
        self.session = session
        self.model = model

    async def get(self, request, *args, **kwargs) -> Response:
        try:
            instance_id = request.match_info['uuid']
        except Exception as err:
            return JsonResponse('Wrong path argument', err, status=400)

        self._validate_instance_id(instance_id)

        try:
            query_results = self.session.query(self.model).filter(self.model.uuid == instance_id).first()
            if not query_results:
                return JsonResponse('Not Found', "", status=404)
            data = self.resource.render_and_encode(query_results)
            return Response(status=200, body=data, content_type='application/json')
        except SQLAlchemyError as err:
            self.session.rollback()
            return JsonResponse("{}", err, status=400)
        except Exception as err:
            self.session.rollback()
            return JsonResponse("Unexpected error: {}", err, status=500)

    @abstractmethod
    async def put(self, request, *args, **kwargs) -> Response:
        pass

    async def delete(self, request, *args, **kwargs) -> Response:
        try:
            instance_id = request.match_info['uuid']
        except Exception as err:
            return JsonResponse('Wrong path argument', err, status=400)

        self._validate_instance_id(instance_id)

        try:
            results_data = self.session.query(self.model).filter(self.model.uuid == instance_id).first()
            if not results_data:
                return JsonResponse(f"Object {instance_id} does not exist", None, status=404)
            self.session.delete(results_data)
            self.session.commit()
        except SQLAlchemyError as err:
            self.session.rollback()
            return JsonResponse("{}", err, status=400)

        return Response(status=204)

    def _validate_instance_id(self, instance_id):
        # TODO: add uuid v4 validator
        if instance_id is None or len(instance_id) > self.MAX_ID_LENGTH:
            return JsonResponse(f"Wrong ID={instance_id} value", None, status=400)

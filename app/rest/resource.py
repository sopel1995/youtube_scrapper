import json
from collections import OrderedDict

from aiohttp.web_urldispatcher import UrlDispatcher

from app.rest.tools import UUIDEncoder


class RestResource:
    def __init__(self, model, factory, collection, properties, id_field, db_session,
                 collection_endpoint_class, instance_endpoint_class, uuid_generator, logger=None, api_version='v1'):
        self.model = model
        self.factory = factory
        self.collection = collection
        self.properties = properties
        self.id_field = id_field

        self.collection_endpoint = collection_endpoint_class(self, db_session, factory, uuid_generator)
        self.instance_endpoint = instance_endpoint_class(self, db_session, factory)
        self.logger = logger
        self.api_version = api_version

    def register(self, router: UrlDispatcher, swagger=True):
        if swagger:
            router.add_route('GET',
                             '/{api_version}/{model}'.format(
                                 api_version=self.api_version,
                                 model=self.model
                             ), self.collection_endpoint.get)

        else:
            router.add_route('*',
                             '/{api_version}/{model}'.format(
                                 api_version=self.api_version,
                                 model=self.model
                             ),
                             self.collection_endpoint.dispatch)

    def render(self, instance):
        return OrderedDict((model, getattr(instance, model)) for model in self.properties)

    @staticmethod
    def encode(data):
        return json.dumps(data, cls=UUIDEncoder, sort_keys=True).encode('utf-8')

    def render_and_encode(self, instance):
        rendered = self.render(instance)
        encoded = self.encode(rendered)
        return encoded

from aiohttp.web_response import Response

from app.rest.tools import RestTools


class JsonResponse(Response):
    CONTENT_TYPE = 'application/json'

    def __init__(self, message, error="", status=200):
        Response.__init__(self, status=status, body=RestTools.normalize_json(message, error),
                          content_type=self.CONTENT_TYPE)

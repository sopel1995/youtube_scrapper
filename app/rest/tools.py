import json


class RestTools:
    @staticmethod
    def normalize_json(message: dict, error: object):
        response_obj = {'reason': error.__class__.__name__}
        if '\n' in str(error):
            error = str(error).split('\n')
            error = error[0]
        error_msg = str(error).replace("'", '')
        if len(error_msg):
            response_obj['status'] = 'error'
        message_text = str(message).replace("'", '"')
        if '{}' in message_text:
            message_text = message_text.replace('{}', '{error}')
            message_text = message_text.format(error=error_msg)
        response_obj['message'] = message_text
        if response_obj['reason'] in ('NoneType', 'str', 'int', 'float'):
            del response_obj['reason']
            if 'status' in response_obj:
                del response_obj['status']
        return json.dumps(response_obj, sort_keys=True)

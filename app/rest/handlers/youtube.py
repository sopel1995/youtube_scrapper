from aiohttp import web
from sqlalchemy.exc import SQLAlchemyError
from app.rest.json_response import JsonResponse
from aiohttp.web_response import Response
from app.model.youtube import Youtube
from app.tools.yt_scraping import YT_scrapper
import json
import uuid


class GeneralWebHandler:
    def __init__(self, logger=None, db_sesion=None):
        self.logger = logger
        self.session = db_sesion
        self.model = Youtube

    async def get_all_datebase(self, request):
        """
        ---
        description:  This endpoint return all database.
        tags:
        - Youtube methods
        produces:
        - text/plain
        responses:
            "200":
                description: successful operation.sudo
            "405":
                description: invalid HTTP Method
        """
        data = []
        try:
            results_data = self.session.query(self.model).all()
            for object in results_data:
                record = {'uuid': (object.uuid,), 'link': (object.link,), 'description': (object.description,),
                          'name': (object.name,),
                          'category': (object.category,), 'access_date': (object.access_date,),
                          'add_date': (object.add_date,), 'search_key': object.search_key}
                data.append(record)

            return Response(status=201, body=json.dumps(data),
                            content_type='application/json')

        except SQLAlchemyError as err:
            self.session.rollback()
            return JsonResponse("{}", err, status=400)
        except Exception as err:
            self.session.rollback()
            return JsonResponse("Unexpected error: {}", err, status=500)

    async def get_key(self, request, *args, **kwargs):
        """
        ---
        description: This endpoint searches for the key movies on youtube and save to database information about them.
        tags:
        - Youtube methods
        parameters:
        - in: path
          name: search_key
          schema:
            type: object
            example:
              uuid: "itil"
            required:
              - search_key
            properties:
              uuid:
                type: string
        produces:
        - text/plain
        responses:
            "201":
                description: successful operation.
            "405":
                description: invalid HTTP Method
        """

        try:
            search_key = request.match_info['search_key']
            yt = YT_scrapper()
            data = yt.video_record(search_key)
            link_list = []
            for record in data:
                model = Youtube(
                    link=record['link'],
                    description=record['description'],
                    name=record['name'],
                    category=record['category'],
                    access_date=record['access_date'],
                    add_date=record['add_date'],
                    search_key=record['search_key']
                )
                model.uuid = str(uuid.uuid4())
                self.session.add(model)
                link_list.append(str(model.link))
            self.session.commit()
            return web.Response(text=f"Added {link_list}", status=201)
        except Exception as err:
            return JsonResponse('Wrong path argument', err, status=400)

    async def get_saved_records_key(self, request, *args, **kwargs):
        """
        ---
        description: This end-point return all records with search_key.
        tags:
        - Youtube methods
        parameters:
        - in: path
          name: search_key
          schema:
            type: object
            example:
              uuid: "itil"
            required:
              - search_key
            properties:
              uuid:
                type: string
        produces:
        - text/plain
        responses:
            "200":
                description: successful operation.
            "405":
                description: invalid HTTP Method
        """

        data = []
        try:
            results_data = self.session.query(self.model).filter(self.model.search_key == request.match_info['search_key']).all()
            for object in results_data:
                record = {'link': (object.link,), 'description': (object.description,), 'name': (object.name,),
                          'category': (object.category,), 'access_date': (object.access_date,),
                          'add_date': (object.add_date,), 'search_key': object.search_key}
                data.append(record)

            return Response(status=200, body=json.dumps(data),
                            content_type='application/json')
        except SQLAlchemyError as err:
            self.session.rollback()
            return JsonResponse("{}", err, status=400)
        except Exception as err:
            self.session.rollback()
            return JsonResponse("Unexpected error: {}", err, status=500)

    async def delete_database(self, request, *args, **kwargs):
        """
        ---
        description: This end-point delete all records with specified search_key.
        tags:
        - Youtube methods
        parameters:
        - in: path
          name: delete_key
          schema:
            type: object
            example:
              delete_key: "itil"
            required:
              - delete_key
            properties:
              delete_key:
                type: string
        responses:
            "204":
                description: successful operation - object deleted
            "400":
                description: problem with path argument or database statement
            "404":
                description: Not Found
        """
        try:
            delete_key = request.match_info['delete_key']
            self.session.query(self.model).filter(self.model.search_key == delete_key).delete()

        except SQLAlchemyError as err:
            self.session.rollback()
            return JsonResponse("{}", err, status=400)
        except Exception as err:
            self.session.rollback()
            return JsonResponse("Unexpected error: {}", err, status=500)

        return web.Response(text=f"Success delete all records with key {delete_key}")

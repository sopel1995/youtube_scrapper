from subprocess import check_output
from bs4 import BeautifulSoup
import re
import json
import sys


class Get_data_with_yt:

    def _get_site_html(self, search_phrase):
        base = 'https://www.youtube.com/results?search_query='
        cmd = 'curl ' + base + search_phrase
        out = check_output(cmd, shell=True)
        return out

    def _get_video_links(self, search_phrase):
        r = self._get_site_html(search_phrase)

        soup = BeautifulSoup(r, 'html5lib')
        # html = soup.prettify('utf-8')

        video_links = []
        all_videos = soup.find_all("a", class_="yt-uix-sessionlink spf-link")
        for link in all_videos:
            video_link = link.get('href')
            if 'watch' in video_link:
                # print(video_link)
                video_links.append(video_link)
        return video_links

    def get_json(self, search_phrase):

        if len(sys.argv) > 1:
            search_phrase = '+'.join(sys.argv[1:])

        video_links = self._get_video_links(search_phrase)
        # print(video_links[0])
        titles = []
        for video_link in video_links:
            title_dict = {}
            title_dict['site_url'] = video_link

            r = self._get_site_html(video_link + '/')
            # print(r)
            soup = BeautifulSoup(r, 'html5lib')
            html = soup.prettify('utf-8')
            # print(html)
            title_html = soup.find(
                class_="yt-uix-tile-link yt-ui-ellipsis yt-ui-ellipsis-2 yt-uix-sessionlink spf-link")
            title = title_html.get('title').strip()
            # print('--------------')
            # print(title)
            title_dict['title'] = title

            description_html = soup.find(class_="yt-lockup-description yt-ui-ellipsis yt-ui-ellipsis-2")
            description = description_html.contents[-1].strip()
            # print(description)
            title_dict['description'] = description

            time_of_movie_html = soup.find(class_="accessible-description")
            time_of_movie = time_of_movie_html.contents[0]
            m = re.search(r'([0-9]+:[0-9]+)', time_of_movie)
            time_of_movie = m.group(0).strip()
            # print(time_of_movie)
            title_dict['time_of_movie'] = time_of_movie

            titles.append(title_dict)
        # print(titles)
        json_dp = json.dumps(titles)
        # print(json_dp)
        return json_dp
